<?php

/**
 * @file
 * Contains bootstrap_classes.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function bootstrap_classes_block_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the bootstrap_classes module.
    case 'help.page.bootstrap_classes_block':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Integrate Bootstrap Classes with the Block Class module.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function bootstrap_classes_block_form_block_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (!empty($form['third_party_settings']['block_class']['classes'])) {
    /** @var \Drupal\block\BlockInterface $block */
    $block = $form_state->getFormObject()->getEntity();
    $block_classes = $block->getThirdPartySetting('block_class', 'classes');
    $block_classes = array_filter(array_map('trim', explode(' ', $block_classes ?? '')));
    /** @var \Drupal\bootstrap_classes\BootstrapClassesInterface $bootstrap_classes_service */
    $bootstrap_classes_service = \Drupal::service('bootstrap_classes');
    $classes = $bootstrap_classes_service->getSelectOptions();
    $flatten_classes = array_merge(...array_values($classes));
    $form['third_party_settings']['block_class']['#type'] = 'details';
    $form['third_party_settings']['block_class']['#title'] = t('Block classes');
    $form['third_party_settings']['block_class']['#open'] = TRUE;
    $form['third_party_settings']['block_class']['bootstrap_classes'] = [
      '#type' => 'select2',
      '#title' => t('Bootstrap classes'),
      '#default_value' => array_intersect($block_classes, $flatten_classes),
      '#options' => $classes,
      '#multiple' => TRUE,
    ];
    $block_classes = array_diff($block_classes, $flatten_classes);
    $form['third_party_settings']['block_class']['classes']['#default_value'] = implode(' ', $block_classes);
    array_unshift($form['actions']['submit']['#submit'], 'bootstrap_classes_block_form_submit');
  }
}

/**
 * Submit handler.
 *
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function bootstrap_classes_block_form_submit($form, FormStateInterface $form_state) {
  $block_classes = $form_state->getValue(['third_party_settings', 'block_class']);
  $block_classes['classes'] .= ' ' . implode(' ', $block_classes['bootstrap_classes']);
  $block_classes['classes'] = trim($block_classes['classes']);
  unset($block_classes['bootstrap_classes']);
  $form_state->setValue(['third_party_settings', 'block_class'], $block_classes);
}
