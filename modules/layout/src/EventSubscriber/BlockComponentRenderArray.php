<?php

namespace Drupal\bootstrap_classes_layout\EventSubscriber;

use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class BlockComponentRenderArraySubscriber.
 */
class BlockComponentRenderArray implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = ['onBuildRender', 50];
    return $events;
  }

  /**
   * Add bootstrap classes to the render array.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    if ($classes = $event->getComponent()->get('bootstrap_classes')) {
      $build = $event->getBuild();
      if (!isset($build['#attributes']['class'])) {
        $build['#attributes']['class'] = [];
      }
      $build['#attributes']['class'] = array_merge($build['#attributes']['class'], $classes);
      $event->setBuild($build);
    }
  }

}
