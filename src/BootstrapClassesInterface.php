<?php

namespace Drupal\bootstrap_classes;

/**
 * Interface BootstrapClassesInterface.
 */
interface BootstrapClassesInterface {

  /**
   * Classes cache identifier.
   */
  const CLASSES_CID = 'bootstrap_classes.classes';

  /**
   * Get bootstrap groups.
   *
   * @return array
   *   The array of translated group labels.
   */
  public function getGroups();

  /**
   * Get bootstrap classes.
   *
   * @return array
   *   Two level array of the grouped bootstrap classes.
   */
  public function getClasses();

  /**
   * Get options array for using in the select element.
   *
   * @return array
   *   The array of options.
   */
  public function getSelectOptions();

}
