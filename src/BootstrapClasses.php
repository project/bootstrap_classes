<?php

namespace Drupal\bootstrap_classes;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class BootstrapClasses.
 */
class BootstrapClasses implements BootstrapClassesInterface {

  use StringTranslationTrait;

  /**
   * The cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * Constructs a new BootstrapClasses object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_default
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(CacheBackendInterface $cache_default, ConfigFactoryInterface $config_factory) {
    $this->cache = $cache_default;
    $this->config = $config_factory->get('bootstrap_classes.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getGroups() {
    return [
      'grid' => $this->t('Grid'),
      'align' => $this->t('Align'),
      'background' => $this->t('Background'),
      'borders' => $this->t('Borders'),
      'display' => $this->t('Display'),
      'embed' => $this->t('Embed'),
      'flex' => $this->t('Flexbox'),
      'float' => $this->t('Float'),
      'overflow' => $this->t('Overflow'),
      'position' => $this->t('Position'),
      'shadows' => $this->t('Shadows'),
      'sizing' => $this->t('Sizing'),
      'spacing' => $this->t('Spacing'),
      'text' => $this->t('Text'),
      'visibility' => $this->t('Visibility'),
      'misc' => $this->t('Misc'),
      'custom' => $this->t('Custom'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getClasses() {
    if ($cached = $this->cache->get(self::CLASSES_CID)) {
      return $cached->data;
    }

    $breakpoints = array_merge([''], $this->config->get('grid_breakpoints'));
    $colors = array_merge($this->config->get('theme_colors'), ['white']);
    $sides = ['', 'top', 'right', 'bottom', 'left'];

    $grid_classes = [
      'container-fluid',
      'row',
      'no-gutters',
      'no-gutters',
    ];
    foreach ($breakpoints as $breakpoint) {
      $grid_classes[] = 'container' . ($breakpoint ? '-' . $breakpoint : '');
    }

    $grid_columns = range(1, $this->config->get('grid_columns'));
    $grid_columns_order = range(0, $this->config->get('grid_columns'));
    $grid_columns_offset = range(1, $this->config->get('grid_columns') - 1);
    $grid_row_columns = range(1, $this->config->get('grid_row_columns'));

    foreach ($breakpoints as $breakpoint) {
      $breakpoint = $breakpoint ? '-' . $breakpoint : '';
      foreach (array_merge(['', 'auto'], $grid_columns) as $modifier) {
        $modifier = $modifier ? '-' . $modifier : '';
        $grid_classes[] = 'col' . $breakpoint . $modifier;
      }
      foreach (array_merge(['first', 'last'], $grid_columns_order) as $modifier) {
        $grid_classes[] = 'order' . $breakpoint . '-' . $modifier;
      }
      foreach ($grid_columns_offset as $modifier) {
        $grid_classes[] = 'offset' . $breakpoint . '-' . $modifier;
      }
      foreach ($grid_row_columns as $modifier) {
        $grid_classes[] = 'row-cols' . $breakpoint . '-' . $modifier;
      }
    }

    $align_classes = [
      'align-baseline',
      'align-top',
      'align-middle',
      'align-bottom',
      'align-text-bottom',
      'align-text-top',
    ];

    $background_classes = [];
    foreach ($colors as $color) {
      $background_classes[] = 'bg-' . $color;
    }
    $background_classes[] = 'bg-transparent';

    $border_classes = [];
    foreach ($sides as $side) {
      $side = $side ? '-' . $side : '';
      foreach (['', 0] as $modifier) {
        $modifier = ($modifier === 0) ? '-0' : '';
        $border_classes[] = 'border' . $side . $modifier;
      }
      $border_classes[] = 'rounded' . $side;
    }
    foreach ($colors as $color) {
      $border_classes[] = 'border-' . $color;
    }
    $border_classes[] = 'rounded-sm';
    $border_classes[] = 'rounded-lg';
    $border_classes[] = 'rounded-circle';
    $border_classes[] = 'rounded-pill';
    $border_classes[] = 'rounded-0';

    $display_classes = [];
    foreach ($breakpoints as $breakpoint) {
      $breakpoint = $breakpoint ? '-' . $breakpoint : '';
      foreach (['none', 'inline', 'inline-block', 'block', 'table', 'table-row', 'table-cell', 'flex', 'inline-flex'] as $modifier) {
        $display_classes[] = 'd' . $breakpoint . '-' . $modifier;
      }
    }

    $embed_classes = [
      'embed-responsive',
      'embed-responsive-item',
    ];

    foreach ($this->config->get('embed_responsive_aspect_ratios') as $modifier) {
      list($x, $y) = explode(' ', $modifier);
      $embed_classes[] = 'embed-responsive-' . $x . 'by' . $y;
    }

    $flex_classes = [];
    foreach ($breakpoints as $breakpoint) {
      $breakpoint = $breakpoint ? '-' . $breakpoint : '';
      foreach (['row', 'column', 'row-reverse', 'column-reverse', 'wrap', 'nowrap', 'wrap-reverse', 'fill', 'grow-0', 'grow-1', 'shrink-0', 'shrink-1'] as $modifier) {
        $flex_classes[] = 'flex' . $breakpoint . '-' . $modifier;
      }
      foreach (['start', 'end', 'center', 'between', 'around'] as $modifier) {
        $flex_classes[] = 'justify-content' . $breakpoint . '-' . $modifier;
      }
      foreach (['start', 'end', 'center', 'baseline', 'stretch'] as $modifier) {
        $flex_classes[] = 'align-items' . $breakpoint . '-' . $modifier;
      }
      foreach (['start', 'end', 'center','between', 'around', 'stretch'] as $modifier) {
        $flex_classes[] = 'align-content' . $breakpoint . '-' . $modifier;
      }
      foreach (['auto', 'start', 'end', 'center', 'baseline', 'stretch'] as $modifier) {
        $flex_classes[] = 'align-self' . $breakpoint . '-' . $modifier;
      }
    }

    $float_classes = [];

    foreach ($breakpoints as $breakpoint) {
      $breakpoint = $breakpoint ? '-' . $breakpoint : '';
      foreach (['left', 'right', 'none'] as $modifier) {
        $float_classes[] = 'float' . $breakpoint . '-' . $modifier;
      }
    }

    $overflow_classes = [
      'overflow-auto',
      'overflow-hidden',
    ];

    $position_classes = [
      'position-static',
      'position-relative',
      'position-absolute',
      'position-fixed',
      'position-sticky',
    ];

    $shadows_classes = [
      'shadow-sm',
      'shadow',
      'shadow-lg',
      'shadow-none',
    ];

    $sizing_classes = [
      'mw-100',
      'mh-100',
      'min-vw-100',
      'min-vh-100',
      'vw-100',
      'vh-100',
    ];
    foreach (['w', 'h'] as $size) {
      foreach ($this->config->get('sizes') as $modifier)
        $sizing_classes[] = $size . '-' . $modifier;
    }

    $spacing_classes = [];
    $spacers = array_merge(range(0, $this->config->get('spacers')), ['auto']);
    foreach ($breakpoints as $breakpoint) {
      $breakpoint = $breakpoint ? '-' . $breakpoint : '';
      foreach ($spacers as $modifier) {
        foreach (['', 'x', 'y', 't', 'l', 'r', 'b'] as $side) {
          if ($modifier !== 'auto') {
            $spacing_classes[] = 'p' . $side . $breakpoint . '-' .  $modifier;
          }
          $spacing_classes[] = 'm' . $side . $breakpoint . '-' . $modifier;
          if ($modifier && $modifier !== 'auto') {
            $spacing_classes[] = 'm' . $side . $breakpoint . '-n' . $modifier;
          }
        }
      }
    }

    $text_classes = [
      'text-monospace',
      'text-justify',
      'text-wrap',
      'text-nowrap',
      'text-truncate',
      'text-lowercase',
      'text-uppercase',
      'text-capitalize',
      'font-weight-light',
      'font-weight-lighter',
      'font-weight-normal',
      'font-weight-bold',
      'font-weight-bolder',
      'font-italic',
      'text-body',
      'text-muted',
      'text-hide',
      'text-decoration-none',
      'text-break',
      'text-reset',
    ];

    foreach ($breakpoints as $breakpoint) {
      $breakpoint = $breakpoint ? '-' . $breakpoint : '';
      foreach (['left', 'right', 'center'] as $modifier) {
        $text_classes[] = 'text' . $breakpoint . '-' . $modifier;
      }
    }
    foreach ($colors as $color) {
      $text_classes[] = 'text-' . $color;
    }

    $visibility_classes = [
      'visible',
      'invisible'
    ];

    $misc_classes = [
      'clearfix',
      'stretched-link',
      'sr-only',
      'sr-only-focusable',
      'user-select-all',
      'user-select-auto',
      'user-select-none',
    ];

    $custom_classes = $this->config->get('custom_classes') ?: [];

    $classes = [
      'grid' => array_combine($grid_classes, $grid_classes),
      'align' => array_combine($align_classes, $align_classes),
      'background' => array_combine($background_classes, $background_classes),
      'borders' => array_combine($border_classes, $border_classes),
      'display' => array_combine($display_classes, $display_classes),
      'embed' => array_combine($embed_classes, $embed_classes),
      'flex' => array_combine($flex_classes, $flex_classes),
      'float' => array_combine($float_classes, $float_classes),
      'overflow' => array_combine($overflow_classes, $overflow_classes),
      'position' => array_combine($position_classes, $position_classes),
      'shadows' => array_combine($shadows_classes, $shadows_classes),
      'sizing' => array_combine($sizing_classes, $sizing_classes),
      'spacing' => array_combine($spacing_classes, $spacing_classes),
      'text' => array_combine($text_classes, $text_classes),
      'visibility' => array_combine($visibility_classes, $visibility_classes),
      'misc' => array_combine($misc_classes, $misc_classes),
      'custom' => array_combine($custom_classes, $custom_classes),
    ];

    $this->cache->set(self::CLASSES_CID, $classes);

    return $classes;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectOptions() {
    $classes = $this->getClasses();
    $groups = $this->getGroups();
    $options = [];
    foreach ($groups as $group_id => $group) {
      $key = (string) $group;
      $options[$key] = $classes[$group_id];
    }
    return array_filter($options);
  }

}
