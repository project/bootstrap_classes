<?php

namespace Drupal\bootstrap_classes\Form;

use Drupal\bootstrap_classes\BootstrapClassesInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BootstrapClassesSettingsForm.
 */
class BootstrapClassesSettingsForm extends ConfigFormBase {

  /**
   * The cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cache;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_default
   *   The cache service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cache_default) {
    parent::__construct($config_factory);
    $this->cache = $cache_default;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bootstrap_classes.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bootstrap_classes_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bootstrap_classes.settings');
    $form['grid_columns'] = [
      '#type' => 'number',
      '#title' => $this->t('Grid columns'),
      '#description' => $this->t('Count of the grid columns.'),
      '#default_value' => $config->get('grid_columns'),
      '#step' => 1,
      '#min' => 1,
      '#required' => TRUE,
    ];
    $form['grid_row_columns'] = [
      '#type' => 'number',
      '#title' => $this->t('Grid row columns'),
      '#description' => $this->t('Count of the grid row columns.'),
      '#default_value' => $config->get('grid_row_columns'),
      '#step' => 1,
      '#min' => 1,
      '#required' => TRUE,
    ];
    $form['grid_breakpoints'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Grid breakpoints'),
      '#description' => $this->t('Specify grid breakpoints. Enter one breakpoint per line.'),
      '#default_value' => $this->implode($config->get('grid_breakpoints')),
      '#required' => TRUE,
    ];
    $form['theme_colors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Theme colors'),
      '#description' => $this->t('Specify theme colors. Enter one color per line.'),
      '#default_value' =>  $this->implode($config->get('theme_colors')),
      '#required' => TRUE,
      '#rows' => 10,
    ];
    $form['spacers'] = [
      '#type' => 'number',
      '#title' => $this->t('Spacers'),
      '#description' => $this->t('Count of the spacers.'),
      '#default_value' => $config->get('spacers'),
      '#step' => 1,
      '#min' => 1,
      '#required' => TRUE,
    ];
    $form['sizes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Sizes'),
      '#description' => $this->t('Specify sizes. Enter one size per line.'),
      '#default_value' =>  $this->implode($config->get('sizes')),
      '#required' => TRUE,
    ];
    $form['embed_responsive_aspect_ratios'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Embed responsive aspect ratios'),
      '#description' => $this->t('Specify embed responsive aspect ratios. Enter one ration per line in the format: "x y", where x and y are integer values.'),
      '#default_value' =>  $this->implode($config->get('embed_responsive_aspect_ratios')),
      '#required' => TRUE,
    ];
    $form['custom_classes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom classes'),
      '#description' => $this->t('Specify custom classes. Enter one custom class per line.'),
      '#default_value' =>  $this->implode($config->get('custom_classes')),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $ratios = $this->explode($form_state->getValue('embed_responsive_aspect_ratios'));
    foreach ($ratios as $ratio) {
      $x_y = explode(' ', $ratio);
      if (count($x_y) !== 2 || $x_y[0] != (int) $x_y[0] || $x_y[1] != (int) $x_y[1] ) {
        $form_state->setErrorByName('embed_responsive_aspect_ratios', $this->t('The ratio should be in the format: "x y", where x and y are integer values.'));
        break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->cache->delete(BootstrapClassesInterface::CLASSES_CID);

    $this->config('bootstrap_classes.settings')
      ->set('grid_columns', $form_state->getValue('grid_columns'))
      ->set('grid_row_columns', $form_state->getValue('grid_row_columns'))
      ->set('grid_breakpoints', $this->explode($form_state->getValue('grid_breakpoints')))
      ->set('theme_colors', $this->explode($form_state->getValue('theme_colors')))
      ->set('spacers', $form_state->getValue('spacers'))
      ->set('sizes', $this->explode($form_state->getValue('sizes')))
      ->set('embed_responsive_aspect_ratios', $this->explode($form_state->getValue('embed_responsive_aspect_ratios')))
      ->set('custom_classes', $this->explode($form_state->getValue('custom_classes')))
      ->save();
  }

  /**
   * @param mixed $value
   *   The value to implode.
   *
   * @return string
   */
  protected function implode($value) {
    if ($value && is_array($value)) {
      return implode(PHP_EOL, $value);
    }
    return '';
  }

  /**
   * @param string $value
   *   The value to explode.
   *
   * @return array
   */
  protected function explode($value) {
    return array_map('trim', explode(PHP_EOL, $value));
  }

}
